from .file import RegisterFileType
from .instance import AddMethodToInstance
from .reporter import RegisterReporter
from .setup import AddSetupItem
from .test import ExtendTest
from .testenity import AddTestEnityMember
from .testrun import ExtendTestRun
from .when import AddWhenFunction
